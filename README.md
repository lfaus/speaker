# Speaking Opportunities

Lee Faus, Global Field CTO at GitLab

Headshots:
![Primary](/photos/103-ZfqQBsgNNz4.jpeg)
![Alternative 1](/photos/004-r4CFd00nE4k.jpeg)
![Alternative 2](/photos/013-CVICHDfPadQ.jpeg)
- [Headshot 2](/photos/001-3PlQM5ANkTA.jpeg)
- [Headshot 5](/photos/033-IZaV9Bo6HPE.jpeg)
- [Headshot 6](/photos/034-8n006nflf2E.jpeg)
- [Headshot 7](/photos/071-ro6n8Wh4M3c.jpeg)
- [Headshot 8](/photos/074-DpewFHu2Ad8.jpeg)
- [Headshot 9](/photos/078-A_N5MH-ORgU.jpeg)
- [Headshot 1](/photos/199-ZJPbC7WvyII.jpeg)
- [Headshot 11](/photos/108-EY5Ve7CEg4s.jpeg)

## Primary Speaker Bio

From debugging teenage attitudes in math class to debugging enterprise systems, my career path has more branches than a Git repository after a hackathon. I've collected certifications like Pokemon cards (gotta catch 'em all from Microsoft to Sun) and saved more at-risk projects than a Silicon Valley therapist on speed dial. After teaching Java when it was still the cool kid on the block, I discovered that software development is less about semicolons and more about convincing humans that AI Agents make better friends than enemies. My superpower? Converting caffeine into code while translating DevOps into human speech for leaders who think 'agile' is just their morning yoga routine. As GitLab's Global Field CTO, I now help organizations automate everything except their coffee breaks, turning 'it works on my machine' into 'it works everywhere.' Three decades of experience has taught me that the most challenging aspects of technology are rarely technical, and that the best kind of digital transformation is the one where everyone gets to be the hero of their own automation story.

## Executive Speaker Bio

Lee has worked with Senior Executives at Fortune 100 and Global 2000 companies for the past 10 years as a trusted advisor around Cloud Adoption, Agile Methodologies, DevSecOps and Engineering Best Practices.  He leverages his experience as an educator to bring complex concepts into a business forum where executives gain valuable advice that can provide immediate impact to their business.  Lee has given numerous keynotes at conferences internationally, speaking from his experience working across different verticals and vocalizing the challenges of running highly performant engineering teams.  Lee brings work experience from Borland, Compuware, RedHat, Alfresco, GitHub, and GitLab and marries that with his consulting experiences at State Farm, Pfizer, Travelport, WellsFargo and Nokia to leverage lessons learned from other executives.  While Lee loves to talk about technology, he is also very passionate about measuring business outcomes and ensuring the efficiency of engineering teams while meeting customer demands.

## Engineering Speaker Bio

Lee has been a software architect, teacher, professor and educator for over 25 years.  He was the first teacher to bring Java to the Association of Computing Machinery (ACM) exam back in 1995.  Lee holds certifications from Novell, Microsoft, Sun, Cisco, Amazon and Google as a practitioner of engineering and operational best practices.  He has run high performing engineering teams with Java/JEE/Spring and improved operational efficiencies with Git, InfraAsCode, PolicyAsCode and CI/CD best practices for Fortune 500 and Global 2000 customers.  He loves to learn and is constantly working with friends, colleagues and OSS communities to bring new technologies to the enterprise.  His experience working in regulated industries allows for a unique perspective for how guardrails can be applied to different technologies to ensure onboarding of different solutions into an organization.  His talks are always well received with his latest talk on GitOps breaking records for the conference for online viewership of 7500+ people.

## Abstracts

See the abstracts folder ...

## Sample Talks

- [GitHub 101](https://www.youtube.com/watch?v=xyTEkNcpyHU)
- [Delivery Free of Charge](https://www.youtube.com/watch?v=6UeXzlwebsM)
- [Git Workflows for Data Science](https://www.youtube.com/watch?v=GOrKCgQjI0U)
- [GitOps 101](https://www.youtube.com/watch?v=eOlBu_I3Ft0)
- [Policy Driven Deployments](https://www.youtube.com/watch?v=6RuUfNAzdGU)

## Writing Samples

- [Policy Driven Deployments](https://www.armory.io/blog/policy-driven-deployments/)
- [ABCs of CD](https://www.armory.io/blog/understanding-the-abcs-of-cd/)
- [GitHub to GitLab - Benefits of the Platform Approach](https://www.linkedin.com/pulse/why-would-github-employee-go-work-gitlab-here-three-reasons-lee-faus)
- [DevOps Platform Business Case](https://about.gitlab.com/blog/2022/02/03/the-devops-platform-series-building-a-business-case/)
